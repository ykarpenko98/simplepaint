//
//  ViewController.swift
//  SimplePaint
//
//  Created by Юрий on 3/4/19.
//  Copyright © 2019 Юрий. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    var lastPoint = CGPoint.zero
    var color = UIColor.black
    var brushWidth: CGFloat = 10.0
    var opacity: CGFloat = 0.85
    
    private var redoLayers = [CALayer]()
    
    var layers: CALayer {
        return viewForLayer.layer
    }
    
    @IBOutlet weak var brashuSlider: UISlider! {
        didSet{
            brashuSlider.transform = CGAffineTransform(rotationAngle: CGFloat(-Double.pi/2))
        }
    }
    @IBOutlet weak var tempImageView: UIImageView!
    
    @IBOutlet weak var viewForLayer: UIView!
    
    @IBOutlet weak var undoBarButtonItem: UIBarButtonItem!
    @IBOutlet weak var redoBarButtonItem: UIBarButtonItem!
    
    @IBAction func sliderDidChagedValue(_ sender: UISlider) {
        brushWidth = CGFloat(sender.value)
    }
    
    @IBAction func chooseColorButton(_ sender: UIButton) {
        if let colorButton = sender.backgroundColor {
            color = colorButton
        }
        
        
    }
    @IBAction func redoBarButtonItem(_ sender: UIBarButtonItem) {
        if let layer = redoLayers.popLast() {
            layers.addSublayer(layer)
            toggleBarButtonItem()
        }
    }
    @IBAction func undoBarButtonItem(_ sender: UIBarButtonItem) {
        if let layer = layers.sublayers?.popLast() {
            redoLayers.append(layer)
            toggleBarButtonItem()
        }
        
    }
    
    
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        //color = UIColor.random // make random color 
        redoLayers.removeAll()
        lastPoint = touch.location(in: view)
    }
    
    override func touchesMoved(_ touches: Set<UITouch>, with event: UIEvent?) {
        guard let touch = touches.first else {
            return
        }
        
        let currentPoint = touch.location(in: view)
        drawLine(from: lastPoint, to: currentPoint)
        lastPoint = currentPoint
        
    }
    
    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        drawLine(from: lastPoint, to: lastPoint)
        
        tempImageView?.image?.draw(in: view.bounds, blendMode: .normal, alpha: opacity)
        
        let layer = CALayer()
        layer.frame = view.bounds
        layer.contents = tempImageView.image?.cgImage
        layer.opacity = Float(opacity)
        layers.addSublayer(layer)
        
        toggleBarButtonItem()
        
        tempImageView.image = nil
    }
    
    private func drawLine(from fromPoint: CGPoint, to toPoint: CGPoint) {
        UIGraphicsBeginImageContext(view.frame.size)
        guard let context = UIGraphicsGetCurrentContext() else {
            return
        }
        
        tempImageView.image?.draw(in: view.bounds)
        
        context.move(to: fromPoint)
        context.addLine(to: toPoint)
        
        context.setLineCap(.round)
        context.setBlendMode(.normal)
        context.setLineWidth(brushWidth)
        context.setStrokeColor(color.cgColor)
        
        context.strokePath()
        
        tempImageView.image = UIGraphicsGetImageFromCurrentImageContext()
        tempImageView.alpha = opacity
        UIGraphicsEndImageContext()
    }
    
    private func toggleBarButtonItem() {
        redoBarButtonItem.isEnabled = redoLayers.count == 0 ?  false : true
        undoBarButtonItem.isEnabled = (layers.sublayers == nil) ? false : true
    }
    
}

extension UIColor {
    static var random: UIColor {
        return UIColor(red: .random(in: 0...1),
                       green: .random(in: 0...1),
                       blue: .random(in: 0...1),
                       alpha: 1.0)
    }
}

